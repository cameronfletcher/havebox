﻿using HaveBox.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HaveBox
{
    internal class DependencyStrapper : IDependencyStrapper
    {
        private readonly IDelegateFactory _delegateFactory;

        internal DependencyStrapper(IDelegateFactory delegateFactory)
        {
            _delegateFactory = delegateFactory;
        }

        public void Strap(IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            depencyMap.Each(x => x.Value.Where(typeDetailsList => typeDetailsList.CreateInstanceDelegate == null)
                .Each(z => z.CreateInstanceDelegate = GetCreateInstance(z, depencyMap)));
        }

        private DelegateFactory.CreateInstance GetCreateInstance(TypeDetails typeDetails, IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            if (typeDetails.IsSingleton && !typeDetails.IsLazySingleton)
            {
                if (typeDetails.CreateInstanceDelegate == null)
                {
                    GetCreateInstanceDelegate(typeDetails, depencyMap).Invoke(typeDetails, out typeDetails.SingletonObject);
                }
                return _delegateFactory.GetCreateInstanceSingleton(typeDetails);
            }

            if (typeDetails.LamdaFunction != null)
            {
                return _delegateFactory.GetCreateInstanceFromLambdaFunction(typeDetails);
            }

            return GetCreateInstanceDelegate(typeDetails, depencyMap);
        }

        private DelegateFactory.CreateInstance GetCreateInstanceDelegate(TypeDetails typeDetails, IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            DependencyCreateInstanceResolver(typeDetails, depencyMap);
            var createInstance = _delegateFactory.GetCreateInstanceDelegate(typeDetails);

            if(typeDetails.IsLazySingleton)
            {
                createInstance = _delegateFactory.GetCreateInstanceLazySingleton(typeDetails, createInstance);
            }

            if (typeDetails.Interceptor != null)
            {
                createInstance = _delegateFactory.GetCreateInstanceWithInterception(typeDetails, createInstance);
            }

            return createInstance;
        }

        private void DependencyCreateInstanceResolver(TypeDetails typeDetails, IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            var constructorParameters = typeDetails.ImplementType.GetConstructors().First().GetParameters();

            int index = 0;
            constructorParameters.Each(x =>
            {
                var parameterTypeDetails = GetParameterTypeDetails(depencyMap, x);

                if (parameterTypeDetails.CreateInstanceDelegate == null)
                {
                    parameterTypeDetails.CreateInstanceDelegate = GetCreateInstance(parameterTypeDetails, depencyMap);
                }

                typeDetails.DependenciesTypeDetails[index] = parameterTypeDetails;
                index++;
            });
        }

        private static TypeDetails GetParameterTypeDetails(IDictionary<Type, IList<TypeDetails>> depencyMap, ParameterInfo parameterInfo)
        {
            var typeDetailsList = depencyMap[parameterInfo.ParameterType];

            if (typeDetailsList.Count() < 2)
            {
                return typeDetailsList.First();
            }

            try
            {
                return typeDetailsList.Single(x => x.ImplementType.Name == parameterInfo.Name); ;
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentOutOfRangeException(string.Format("Cannot find an implementation called {0} for {1}", parameterInfo.Name, parameterInfo.ParameterType));
            }
        }
    }
}
