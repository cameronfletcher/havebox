﻿using HaveBox.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HaveBox
{
    public class Container : IContainer
    {
        internal IDictionary<Type, IList<TypeDetails>> _depencyMap;
        internal IDictionary<Type, TypeDetails> _depencyFastMap;
        private readonly IDependencyStrapper _dependencyStrapper;
        protected Guid _containerId;

        public Container()
        {
            _depencyMap = new Dictionary<Type, IList<TypeDetails>>();
            _depencyFastMap = new Dictionary<Type, TypeDetails>();
            _dependencyStrapper = new DependencyStrapper(new DelegateFactory());
            _containerId = Guid.NewGuid();

#if !SILVERLIGHT
            new Config(_depencyMap).MergeConfig<ConfigInjectionConfig>();
            StrapAndMarkSingletons(_containerId, _depencyMap);
#endif
        }

        public virtual void Configure(Action<IConfig> registry)
        {
            registry.Invoke(new Config(_depencyMap));
            StrapAndMarkSingletons(_containerId, _depencyMap);
        }

        private void StrapAndMarkSingletons(Guid containerId, IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            _dependencyStrapper.Strap(depencyMap);
            _depencyMap.Each(x => _depencyFastMap[x.Key] = x.Value.First());

            MarkSingletionsWithContainerId(containerId, depencyMap);
        }

        public virtual TYPE GetInstance<TYPE>()
        {
            return (TYPE) GetInstance(typeof(TYPE));
        }

        public virtual object GetInstance(Type type)
        {
            object instance;
            var typeDetails = GetTypeDetails(type);
            typeDetails.CreateInstanceDelegate(typeDetails, out instance);
            return instance;
        }

        public virtual TYPE GetInstance<TYPE>(string implementationName)
        {
            return (TYPE) GetInstance(typeof(TYPE), implementationName);
        }

        public virtual object GetInstance(Type type, string implementationName)
        {
            object instance;
            var typeDetails = GetTypeDetails(type, implementationName);
            typeDetails.CreateInstanceDelegate(typeDetails, out instance);
            return instance;
        }

        internal TypeDetails GetTypeDetails(Type type)
        {
            try
            {
                return _depencyFastMap[type];
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentOutOfRangeException(string.Format("Cannot find  {0}", type.Name));
            }
        }

        internal TypeDetails GetTypeDetails(Type type, string implementationName)
        {
            try
            {
                return _depencyMap[type].Single(x => x.ImplementType.Name == implementationName);
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentOutOfRangeException(string.Format("Cannot find  {0}", type.Name));
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentOutOfRangeException(string.Format("Cannot find an implementation called {0} for {1}", implementationName, type.Name));
            }
        }

        public virtual IDisposableContainer SpawnContainerAndKeepSingletons()
        {
            return new DisposableContainer(_depencyMap);
        }

        public IDisposableContainer SpawnContainer()
        {
            return new DisposableContainer(CreateMapWithNewSingletons(_depencyMap));
        }

        internal void MarkSingletionsWithContainerId(Guid containerId, IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            depencyMap.Each(typeKey =>
            {
                typeKey.Value.Where(typeDetail => typeDetail.SingletonOwner == default(Guid)).Each(typeDetail =>
                {
                    typeDetail.SingletonOwner = containerId;
                });
            });
        }

        private IDictionary<Type, IList<TypeDetails>> CreateMapWithNewSingletons(IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            var mapWithNewSingletons = new Dictionary<Type, IList<TypeDetails>>();

            depencyMap.Each(typeKey =>
                {
                    mapWithNewSingletons[typeKey.Key] = new List<TypeDetails>();
                    typeKey.Value.Each(typeDetail =>
                    {
                        if (!typeDetail.IsSingleton)
                        {
                            mapWithNewSingletons[typeKey.Key].Add(typeDetail);
                        }
                        else
                        {
                            var implementationClone = (TypeDetails)typeDetail.Clone();
                            implementationClone.CreateInstanceDelegate = null;
                            implementationClone.SingletonObject = null;
                            implementationClone.SingletonOwner = default(Guid);
                            mapWithNewSingletons[typeKey.Key].Add(implementationClone);
                        }
                    });
                });

            _dependencyStrapper.Strap(mapWithNewSingletons);
            return mapWithNewSingletons;
        }
    }
}
