﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace HaveBox
{
    internal static class IEnumerableExtensions
    {
        public static void Each<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
            }
        }
#if !SILVERLIGHT
        public static IEnumerable<KeyValuePair<string, string>> ToIEnumerable(this NameValueCollection source)
        {
            return source.AllKeys.SelectMany(source.GetValues, (k, v) => new KeyValuePair<string, string>(k, v));
        }
#endif
    }
}
