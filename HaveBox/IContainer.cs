﻿using HaveBox.Configuration;
using System;

namespace HaveBox
{
    public interface IContainer
    {
        void Configure(Action<IConfig> registry);
        TYPE GetInstance<TYPE>();
        TYPE GetInstance<TYPE>(string implementationName);
        object GetInstance(Type type);
        object GetInstance(Type type, string implementationName);
        IDisposableContainer SpawnContainerAndKeepSingletons();
        IDisposableContainer SpawnContainer();
    }
}
