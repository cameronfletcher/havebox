﻿using System;
namespace HaveBox.Configuration
{
    public class InjectionProperty : IInjectionProperty
    {
        private readonly TypeDetails _typeDetails;

        internal InjectionProperty(TypeDetails typeDetails)
        {
            _typeDetails = typeDetails;
        }

        public IInjectionProperty AsSingleton()
        {
            _typeDetails.IsSingleton = true;
            _typeDetails.IsLazySingleton = false;
            return new InjectionProperty(_typeDetails);
        }

        public IInjectionProperty AndInterceptInstantiationWith<TYPE>()
        {
            return AndInterceptInstantiationWith((IInstantiationInterceptor)typeof(TYPE).GetConstructor(Type.EmptyTypes).Invoke(Type.EmptyTypes));
        }

        public IInjectionProperty AndInterceptInstantiationWith(IInstantiationInterceptor interceptor)
        {
            _typeDetails.Interceptor = interceptor;
            return new InjectionProperty(_typeDetails);
        }

        public IInjectionProperty AsLazySingleton()
        {
            _typeDetails.IsSingleton = true;
            _typeDetails.IsLazySingleton = true;
            return new InjectionProperty(_typeDetails);
        }
    }
}