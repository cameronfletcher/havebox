﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace HaveBox.Configuration.Scanners
{
    public class ScannerSelector : IScannerSelector
    {
        private readonly IDictionary<Type, IList<TypeDetails>> _depencyMap;
        private readonly Assembly _assembly;

        internal ScannerSelector(IDictionary<Type, IList<TypeDetails>> depencyMap, Assembly assembly)
        {
            _depencyMap = depencyMap;
            _assembly = assembly;
        }
        public void UsingSimpleScanner()
        {
            new SimpleScanner(_depencyMap).Scan(_assembly);
        }
    }
}
