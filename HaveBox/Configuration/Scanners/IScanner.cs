﻿using System.Reflection;

namespace HaveBox.Configuration.Scanners
{
    internal interface IScanner
    {
        void Scan(Assembly assembly);
    }
}
