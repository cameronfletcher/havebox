﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace HaveBox.Configuration.Scanners
{
    internal class SimpleScanner : IScanner
    {
        private readonly IDictionary<Type, IList<TypeDetails>> _depencyMap;

        public SimpleScanner(IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            _depencyMap = depencyMap;
        }

        public void Scan(Assembly assembly)
        {
            var registry = new Config(_depencyMap);

            assembly.GetTypes().Each(type =>
                {
                    type.GetInterfaces().Each(interfaze =>
                        {
                            registry.For(interfaze).Use(type);
                        });
                });
        }
    }
}
