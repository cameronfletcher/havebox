﻿using HaveBox.Configuration.Scanners;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HaveBox.Configuration
{
    public class Config : IConfig 
    {
        private IDictionary<Type, IList<TypeDetails>> _depencyMap;

        public Config()
        {
        }

        internal Config(IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            _depencyMap = depencyMap;
        }

        public IInjectionExpression For<TYPE>()
        {
            return For(typeof(TYPE));
        }

        public IInjectionExpression For(Type type)
        {
            CreateDepencyMapIfNeeded();

            IList<TypeDetails> dependencies;

            if (!_depencyMap.TryGetValue(type, out dependencies))
            {
                dependencies = new List<TypeDetails>();
                _depencyMap[type] = dependencies;
            }

            return new InjectionExpression(dependencies);
        }

        public IScannerSelector Scan(Assembly assembly)
        {
            CreateDepencyMapIfNeeded();
            return new ScannerSelector(_depencyMap, assembly);
        }

        public void MergeConfig<CONFIG>()
        {
            MergeConfig((IConfig)typeof(CONFIG).GetConstructor(Type.EmptyTypes).Invoke(Type.EmptyTypes));
        }

        public void MergeConfig(IConfig config)
        {
            CreateDepencyMapIfNeeded();

            ((Config)config).GetDepencyMap().Each(x => _depencyMap[x.Key] = x.Value);
        }

        private void CreateDepencyMapIfNeeded()
        {
            if (_depencyMap == default(Dictionary<Type, IList<TypeDetails>>))
            {
                _depencyMap = new Dictionary<Type, IList<TypeDetails>>();
            }
        }

        internal IDictionary<Type, IList<TypeDetails>> GetDepencyMap()
        {
            return _depencyMap;
        }
    }
}
