﻿namespace HaveBox.Configuration
{
    public interface IInjectionProperty
    {
        IInjectionProperty AsSingleton();
        IInjectionProperty AsLazySingleton();
        IInjectionProperty AndInterceptInstantiationWith<TYPE>();
        IInjectionProperty AndInterceptInstantiationWith(IInstantiationInterceptor interceptor);
    }
}