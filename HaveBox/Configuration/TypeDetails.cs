﻿using System;

namespace HaveBox.Configuration
{
#if SILVERLIGHT
    public class TypeDetails
#else
    internal class TypeDetails
#endif
    {
        public DelegateFactory.CreateInstance CreateInstanceDelegate;
        public TypeDetails[] DependenciesTypeDetails;
        public Type ImplementType;
        public bool IsSingleton;
        public bool IsLazySingleton;
        public object SingletonObject;
        public Func<object> LamdaFunction;
        public Guid SingletonOwner;
        public IInstantiationInterceptor Interceptor;

        public object Clone()
        {
            return new TypeDetails
            {
                CreateInstanceDelegate = CreateInstanceDelegate,
                DependenciesTypeDetails = DependenciesTypeDetails,
                ImplementType = ImplementType,
                IsSingleton = IsSingleton,
                SingletonObject = SingletonObject,
                LamdaFunction = LamdaFunction,
                SingletonOwner = SingletonOwner,
            };
        }
    }
}