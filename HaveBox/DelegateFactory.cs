﻿using HaveBox.Configuration;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection;

namespace HaveBox
{
#if SILVERLIGHT
    public class DelegateFactory : IDelegateFactory
#else
    internal class DelegateFactory : IDelegateFactory
#endif
    {
        public CreateInstance GetCreateInstanceFromLambdaFunction(TypeDetails typeDetailsLocal)
        {
            return (TypeDetails typeDetails, out object output) => output = typeDetailsLocal.LamdaFunction.Invoke();
        }

        public CreateInstance GetCreateInstanceSingleton(TypeDetails typeDetailsLocal)
        {
            return (TypeDetails typeDetails, out object output) => output = typeDetailsLocal.SingletonObject;
        }

        public CreateInstance GetCreateInstanceLazySingleton(TypeDetails typeDetailsLocal, CreateInstance instansiationDelegate)
        {
            return (TypeDetails typeDetails, out object output) =>
                {
                    instansiationDelegate(typeDetails, out output);
                    typeDetails.IsSingleton = true;
                    typeDetails.SingletonObject = output;
                    typeDetails.CreateInstanceDelegate = GetCreateInstanceSingleton(typeDetails);
                };
        }

        public CreateInstance GetCreateInstanceWithInterception(TypeDetails typeDetailsLocal, CreateInstance delegateToBeIncepted)
        {
            var argTypes = typeDetailsLocal.DependenciesTypeDetails.Select(typeDetails => typeDetails.ImplementType);
            var createInterceptedInstance = GetCreateInterceptedInstance(typeDetailsLocal);

            return (TypeDetails typeDetails, out object output) =>
                {
                    var invocation = new Instantiation(typeDetails, delegateToBeIncepted, createInterceptedInstance)
                    {
                        argTypes = argTypes,
                        DependenciesTypeDetails = typeDetails.DependenciesTypeDetails,
                    };

                    typeDetailsLocal.Interceptor.Intercept(invocation);
                    output = invocation.instance;
                };
        }

        public CreateInterceptedInstance GetCreateInterceptedInstance(TypeDetails typeDetails)
        {
#if SILVERLIGHT
            var method = new DynamicMethod("CreateInterceptedInstance" + typeDetails.ImplementType.Name, typeof (void), new[] {typeof (object[]), typeof (object).MakeByRefType()});
#else
            var method = new DynamicMethod("CreateInterceptedInstance" + typeDetails.ImplementType.Name, MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard, typeof(void), new[] { typeof(object[]), typeof(object).MakeByRefType() }, typeof(TypeDetails), true);
#endif
            var ilGenerator = method.GetILGenerator();
            var parameters = typeDetails.ImplementType.GetConstructors().First().GetParameters();

            ilGenerator.Emit(OpCodes.Ldarg_1);

            var index = 0;
            parameters.Each(x =>
            {
                ilGenerator.Emit(OpCodes.Ldarg_0);
                ilGenerator.Emit(OpCodes.Ldc_I4, index);
                ilGenerator.Emit(OpCodes.Ldelem_Ref);
                ilGenerator.Emit(OpCodes.Castclass, x.ParameterType);
                index++;
            });

            ilGenerator.Emit(OpCodes.Newobj, typeDetails.ImplementType.GetConstructors().First());
            ilGenerator.Emit(OpCodes.Stind_Ref);
            ilGenerator.Emit(OpCodes.Ret);

            return (CreateInterceptedInstance)method.CreateDelegate(typeof(CreateInterceptedInstance));
        }

        public CreateInstance GetCreateInstanceDelegate(TypeDetails typeDetails)
        {
#if SILVERLIGHT
            var method = new DynamicMethod("CreateInstance" + typeDetails.ImplementType.Name, typeof (void), new[] {typeof (TypeDetails), typeof (object).MakeByRefType()});
#else
            var method = new DynamicMethod("CreateInstance" + typeDetails.ImplementType.Name, MethodAttributes.Public | MethodAttributes.Static, CallingConventions.Standard, typeof(void), new[] { typeof(TypeDetails), typeof(object).MakeByRefType() }, typeof(TypeDetails), true);
#endif
            var ilGenerator = method.GetILGenerator();
            var parameters = typeDetails.ImplementType.GetConstructors().First().GetParameters();

            LocalBuilder[] locals = null;
            int index = 0;
            if (parameters.Any())
            {
                ilGenerator.DeclareLocal(typeof(TypeDetails));
                locals = parameters.Select(x => ilGenerator.DeclareLocal(typeDetails.DependenciesTypeDetails[index].DependenciesTypeDetails.Any() ? typeof(object) : x.GetType())).ToArray();
            }
            
            index = 0;
            parameters.Each(x =>
            {
                ilGenerator.Emit(OpCodes.Ldarg_0);
                ilGenerator.Emit(OpCodes.Ldfld, typeof(TypeDetails).GetField("DependenciesTypeDetails"));
                ilGenerator.Emit(OpCodes.Stloc, 0);

                if (typeDetails.DependenciesTypeDetails[index].IsSingleton && !typeDetails.DependenciesTypeDetails[index].IsLazySingleton)
                {
                    ilGenerator.Emit(OpCodes.Ldloc, 0);
                    ilGenerator.Emit(OpCodes.Ldc_I4, index);
                    ilGenerator.Emit(OpCodes.Ldelem_Ref);
                    ilGenerator.Emit(OpCodes.Ldfld, typeof(TypeDetails).GetField("SingletonObject"));
                    ilGenerator.Emit(OpCodes.Stloc, index+1);
                }
                else if (typeDetails.DependenciesTypeDetails[index].DependenciesTypeDetails.Any())
                {
                    ilGenerator.Emit(OpCodes.Ldloc, 0);
                    ilGenerator.Emit(OpCodes.Ldc_I4, index);
                    ilGenerator.Emit(OpCodes.Ldelem_Ref);
                    ilGenerator.Emit(OpCodes.Ldfld, typeof(TypeDetails).GetField("CreateInstanceDelegate"));
                    ilGenerator.Emit(OpCodes.Ldloc, 0);
                    ilGenerator.Emit(OpCodes.Ldc_I4, index);
                    ilGenerator.Emit(OpCodes.Ldelem_Ref);
                    ilGenerator.Emit(OpCodes.Ldloca_S, index+1);
                    ilGenerator.Emit(OpCodes.Call, typeof(CreateInstance).GetMethod("Invoke", new[] { typeof(TypeDetails), typeof(object).MakeByRefType() }));
                }
                else
                {
                    ilGenerator.Emit(OpCodes.Newobj, typeDetails.DependenciesTypeDetails[index].ImplementType.GetConstructors().First());
                    ilGenerator.Emit(OpCodes.Stloc, index+1);
                }
                index++;
            });

            ilGenerator.Emit(OpCodes.Ldarg_1);

            index = 0;
            parameters.Each(x =>
            {
                ilGenerator.Emit(OpCodes.Ldloc, locals[index]);

                if (typeDetails.DependenciesTypeDetails[index].DependenciesTypeDetails.Any())
                {
                    ilGenerator.Emit(OpCodes.Castclass, x.ParameterType);
                }

                index++;
            });

            ilGenerator.Emit(OpCodes.Newobj, typeDetails.ImplementType.GetConstructors().First());
            ilGenerator.Emit(OpCodes.Stind_Ref);
            ilGenerator.Emit(OpCodes.Ret);

            return (CreateInstance) method.CreateDelegate(typeof (CreateInstance));
        }

#if SILVERLIGHT
        public delegate void CreateInstance(TypeDetails typeDetails, out object output);
        public delegate void CreateInterceptedInstance(object[] instances, out object output);
#else
        internal delegate void CreateInstance(TypeDetails typeDetails, out object output);
        internal delegate void CreateInterceptedInstance(object[] instances, out object output);
#endif
    }
}