﻿using HaveBox.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HaveBox
{
    internal class DisposableContainer : Container, IDisposableContainer
    {
        IList<object> _disposeAbleInstances;

        public DisposableContainer(IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            _depencyMap = depencyMap;
            _disposeAbleInstances = new List<object>();
            _containerId = Guid.NewGuid();
            MarkSingletionsWithContainerId(_containerId, _depencyMap);
            RegisterDisposeableSingletons(depencyMap);
            _depencyMap.Each(x => _depencyFastMap[x.Key] = x.Value.First());
        }

        public override void Configure(Action<IConfig> registry)
        {
            base.Configure(registry);
            RegisterDisposeableSingletons(_depencyMap);
        }

        private void RegisterDisposeableSingletons(IDictionary<Type, IList<TypeDetails>> depencyMap)
        {
            depencyMap.Each(type =>
                    type.Value.Each(typeDetails =>
                        {
                            if (typeDetails.IsSingleton && typeDetails.SingletonOwner == _containerId && typeDetails.SingletonObject is IDisposable)
                            {
                                _disposeAbleInstances.Add(typeDetails.SingletonObject);
                            }
                        }));
        }

        public override object GetInstance(Type type)
        {
            var typeDetails = GetTypeDetails(type);
            return GetInstanceAndRegisterDiposable(typeDetails);
        }

        public override object GetInstance(Type type, string implementationName)
        {
            var typeDetails = GetTypeDetails(type, implementationName);
            return GetInstanceAndRegisterDiposable(typeDetails);
        }

        private object GetInstanceAndRegisterDiposable(TypeDetails typeDetails)
        {
            object instance;
            typeDetails.CreateInstanceDelegate(typeDetails, out instance);

            if (instance is IDisposable && (!typeDetails.IsSingleton || typeDetails.SingletonOwner == _containerId))
            {
                _disposeAbleInstances.Add(instance);
            }
            return instance;
        }

        public void Dispose()
        {
            _disposeAbleInstances.Distinct()
                .Cast<IDisposable>()
                .Each(instance => instance.Dispose());

            GC.SuppressFinalize(this);
        }

        public void DisposeInstance(IDisposable instance)
        {
            _disposeAbleInstances = _disposeAbleInstances.Distinct().ToList();
            if (_disposeAbleInstances.Remove(instance))
            {
                instance.Dispose();
            }
        }
    }
}
