﻿using FluentAssertions;
using HaveBox;
using System;
using Xunit;

namespace HaveBoxUnitTests
{
    public class SpawnContainerAndKeepSingletonsTests
    {
        static public bool _diposeWasCalled;

        public interface INoDependency1 : IDisposable
        {
            Guid GetClassGuid();
        }

        public class NoDependencyClass1 : INoDependency1, IDisposable
        {
            private readonly Guid _classGuid;

            public NoDependencyClass1()
            {
                _classGuid = Guid.NewGuid();
            }

            public Guid GetClassGuid()
            {
                return _classGuid;
            }

            public void Dispose()
            {
                _diposeWasCalled = true;
            }
        }

        private readonly Container _container;

        public SpawnContainerAndKeepSingletonsTests()
        {
            _container = new Container();
            _diposeWasCalled = false;
        }

        [Fact]
        public void Given_A_Container_And_A_Spawned_Container_With_Same_Singletons_When_Getting_A_Singleton_Instance_Then_The_Instance_Is_The_Same()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton());
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = spawnedContainer.GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_Container_And_A_Spawned_Container_With_Same_Lazy_Singletons_When_Getting_A_Lazy_Singleton_Instance_Then_The_Instance_Is_The_Same()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsLazySingleton());
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();

            var instance1 = _container.GetInstance<INoDependency1>();
            var instance2 = spawnedContainer.GetInstance<INoDependency1>();

            instance1.GetClassGuid().Should().Be(instance2.GetClassGuid());
        }

        [Fact]
        public void Given_A_IDisposeable_Singleton_When_Disposing_The_Container_Then_The_Singletons_Dispose_Method_Is_Called()
        {
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton());
            spawnedContainer.GetInstance<INoDependency1>();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeTrue();
        }

        [Fact]
        public void Given_A_IDisposeable_Lazy_Singleton_When_Disposing_The_Container_Then_The_Lazy_Singletons_Dispose_Method_Is_Called()
        {
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsLazySingleton());
            spawnedContainer.GetInstance<INoDependency1>();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeTrue();
        }

        [Fact]
        public void Given_A_IDisposeable_Owned_Transient_When_Disposing_The_Container_Then_The_Transients_Dispose_Method_Is_Called()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.GetInstance<INoDependency1>();
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeTrue();
        }

        [Fact]
        public void Given_A_IDisposeable_Singleton_When_Disposing_The_Container_Right_After_Creation_Then_The_Singletons_Dispose_Method_Is_Called()
        {
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton());
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeTrue();
        }

        [Fact]
        public void Given_A_IDisposeable_Lazy_Singleton_When_Disposing_The_Container_Right_After_Creation_Then_The_Lazy_Singletons_Dispose_Method_Is_Not_Called()
        {            
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsLazySingleton());
            _diposeWasCalled = false;

            spawnedContainer.Dispose();

            _diposeWasCalled.Should().BeFalse();
        }

        [Fact]
        public void Given_A_IDisposeable_Singleton_Instance_When_Disposing_The_Instance_Then_The_Singletons_Dispose_Method_Is_Called()
        {
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsSingleton());
            _diposeWasCalled = false;

            spawnedContainer.DisposeInstance(spawnedContainer.GetInstance<INoDependency1>());

            _diposeWasCalled.Should().BeTrue();
        }

        [Fact]
        public void Given_A_IDisposeable_Lazy_Singleton_Instance_When_Disposing_The_Instance_Then_The_Lazy_Singletons_Dispose_Method_Is_Not_Called()
        {
            _container.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>().AsLazySingleton());
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            _diposeWasCalled = false;

            spawnedContainer.DisposeInstance(spawnedContainer.GetInstance<INoDependency1>());

            _diposeWasCalled.Should().BeFalse();
        }

        [Fact]
        public void Given_A_IDisposeable_Owned_Transient_Instance_When_Disposing_The_Instance_Then_The_Transients_Dispose_Method_Is_Called()
        {
            var spawnedContainer = _container.SpawnContainerAndKeepSingletons();
            spawnedContainer.Configure(x => x.For<INoDependency1>().Use<NoDependencyClass1>());
            _diposeWasCalled = false;

            spawnedContainer.DisposeInstance(spawnedContainer.GetInstance<INoDependency1>());

            _diposeWasCalled.Should().BeTrue();
        }
    }
}
