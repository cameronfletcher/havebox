﻿using HaveBox;
using Xunit;
using FluentAssertions;
using System;
using System.Configuration;
using Xunit.Extensions;

namespace HaveBoxUnitTests
{
    public class ConfigInjectionTests
    {

        public interface INoDependency1
        {
            Guid GetClassGuid();
        }

        public class NoDependencyClass1 : INoDependency1
        {
            private readonly Guid _classGuid;

            public NoDependencyClass1(IKeyValueSet NoDependencyClass1Guid)
            {
                _classGuid = new Guid(NoDependencyClass1Guid.Value);
            }

            public Guid GetClassGuid()
            {
                return _classGuid;
            }
        }

        private readonly Container _container;

        public ConfigInjectionTests()
        {
            _container = new Container();
        }

        [Theory]
        [InlineData("Test", "TestValue")]
        [InlineData("TestA", "TestValueA")]
        public void Given_A_Container_With_Configs_When_Getting_A_Config_Then_The_Right_Config_Is_Retrived(string key, string value)
        {
            var keyValueSet = _container.GetInstance<IKeyValueSet>(key);

            keyValueSet.Key.Should().Be(key);
            keyValueSet.Value.Should().Be(value);
        }

        [Fact]
        public void Given_A_Container_With_An_Object_With_Config_Injection_When_Getting_Instance_Then_Config_Is_Resolved()
        {
            _container.Configure(config => config.For<INoDependency1>().Use<NoDependencyClass1>());

            var instance = _container.GetInstance<INoDependency1>();

            instance.GetClassGuid().Should().Be("0E340847-1AB6-4D51-B80E-FC6F052F7580");
        }

        [Fact]
        public void Given_A_Container_With_Configs_When_Getting_A_Non_Exsisting_Config_Then_An_Exception_Is_Thrown()
        {
            Action keyValueSet = () => _container.GetInstance<IKeyValueSet>("NonExsistingKey");

            keyValueSet.ShouldThrow<ArgumentOutOfRangeException>();
        }
    }
}
